//go:generate go run ../../../tools/generate_plugindata/main.go
//go:generate go run ../../../tools/generate_plugindata/main.go --clean
package conso

import (
	"encoding/base64"
	"errors"
	"regexp"
	"strconv"
	"strings"

	"github.com/influxdata/telegraf"
	"github.com/influxdata/telegraf/plugins/inputs"
	"golang.org/x/crypto/ssh"
)

type Conso struct {
	Host     string          `toml:"Host"`
	Port     string          `toml:"Port"`
	Log      telegraf.Logger `toml:"-"`
	AuthMode string          `toml:"AuthMode"`
	User     string          `toml:"User"`
	Password string          `toml:"Password"`
}

func (c *Conso) Description() string {
	return "check watt using powertop sshing tghout machine  "
}

// Init is for setup, and validating config.
func (c *Conso) Init() error {
	return nil
}

func (c *Conso) Gather(acc telegraf.Accumulator) error {
	password, err := base64.StdEncoding.DecodeString(c.Password)
	if err != nil {
		c.Log.Errorf("decoding password  ", err)
		return err
	}
	client, session, err := connectToHost(c.User, c.Host+":"+c.Port, string(password))
	if err != nil {
		c.Log.Errorf("error connecting to  host  "+c.Host, err)
		return err
	}
	defer client.Close()
	var command string
	switch c.AuthMode {
	case "sudo":
		command = "echo password  | sudo -s powertop -C ; grep  baseline 'powertop.csv' "
	case "sudo_passwordless":
		command = " sudo powertop -C ; grep  baseline 'powertop.csv' "
	case "root":
		command = " powertop -C ; grep  baseline 'powertop.csv' "
	default:
		{
			err = errors.New("error  selecting command ensure that you specified a valid authmode")
			c.Log.Errorf("", err)
			return err
		}
	}

	out, err := session.CombinedOutput(command)
	if err != nil {
		c.Log.Errorf("error greppring csv ", err)
		return err
	}
	outputstr := string(out)
	//  The system baseline power is estimated at:  20.0  W;
	what := strings.Split(outputstr, ":")[1]
	// ⬆ on récupère la partie chiffre , unit    les espases et  le ;  ==>   20.0  W;
	newvalue := strings.Replace(what, ";", "", -1)
	//⬆ on retire le point virgule ==>   20.0  W
	newvalue = strings.Replace(newvalue, " ", "", -1)
	//⬆ on retire les espaces ==>20.0W
	newvalue = strings.Replace(newvalue, `\n`, "", -1)
	//⬆ on retire les saut de lignes ==> 20.0W
	regexUnit := regexp.MustCompile("[a-zA-Z]")
	//⬆ on créer une regex qui ne prends que les lettres
	unittable := regexUnit.FindAllString(newvalue, -1)
	//⬆ on récupère un tableau avec toutes les lettres
	unit := ""
	for i := 0; i < len(unittable); i++ {
		unit += unittable[i]
		// on recolle les lettres ==> W
	}
	number, err := strconv.ParseFloat(newvalue[0:len(newvalue)-len(unit)-1], 64)
	//
	if err != nil {
		c.Log.Errorf(" error casting number  csv %v ", err)
		return err

	}
	var convertedvalue float64
	convertedvalue = -1
	switch unit {
	case "mW":
		convertedvalue = number / 1000
	case "µW":
		convertedvalue = number / 1000000
	case "W":
		convertedvalue = number
	default:
		c.Log.Errorf("conversion failed  unit is %v ", unit)
		return err
	}

	fields := make(map[string]interface{})
	// tags := make(map[string]string)
	// tags["conso"] = "Conso"
	fields[c.Host] = convertedvalue
	acc.AddFields("conso", fields, nil)
	return nil

}

func init() {
	inputs.Add("conso", func() telegraf.Input { return &Conso{} })
}

func connectToHost(user, host, pass string) (*ssh.Client, *ssh.Session, error) {

	sshConfig := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{ssh.Password(pass)},
	}
	sshConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	client, err := ssh.Dial("tcp", host, sshConfig)
	if err != nil {
		return nil, nil, err
	}

	session, err := client.NewSession()
	if err != nil {
		client.Close()
		return nil, nil, err
	}

	return client, session, nil
}
