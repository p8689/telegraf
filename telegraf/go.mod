module gitlab.com/p8689/telegraf/telegraf

go 1.15

require (
	github.com/influxdata/telegraf v1.22.4
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e
)
