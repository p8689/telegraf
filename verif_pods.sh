#!bin/bash
NAMESPACE=$1
SERVICE=$2
GREP="grep -v -q -i"
if kubectl get pod -n $NAMESPACE | grep $SERVICE | cut -d" " -f9 | $GREP running
then 
        exit 1
fi