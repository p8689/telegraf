#!bin/bash
IMAGE=$1
ID_CONTAINER=$(docker run -d $IMAGE)
sleep 10
LINE=$(docker ps --filter id=$ID_CONTAINER | wc -l)
docker rm -f $ID_CONTAINER
if [ $LINE -ne 2 ]
then
        exit 1
fi
